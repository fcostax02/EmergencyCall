package br.com.interage.emergencycall.utils;

import android.content.Context;
import android.text.Editable;
import android.text.TextUtils;
import android.view.View;
import android.widget.EditText;

import br.com.interage.emergencycall.R;

public class Validator {

	public static boolean validateNotNull(Context context,View pView) {
		if (pView instanceof EditText) {
			EditText edText = (EditText) pView;
			Editable text = edText.getText();
			if (text != null) {
				String strText = text.toString();
				if (!TextUtils.isEmpty(strText)) {
					return true;
				}
			}
			// Em qualquer outra condição é gerado um erro
			edText.setError(context.getString(R.string.error_field_required));
			edText.setFocusable(true);
			edText.requestFocus();
			return false;
		}
		return false;
	}

	public static boolean validateEmail(Context context , EditText editText) {
		String email = editText.getText().toString();
		if (TextUtils.isEmpty(email)) {
			return false;
		} else {
			boolean matches = android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches();
			if (!matches) {
				editText.setError(context.getString(R.string.error_invalid_email));
				editText.setFocusable(true);
				editText.requestFocus();				
			} else {

			}
			return matches;
		}
	}
}
