package br.com.interage.emergencycall.utils;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.widget.Toast;

import br.com.interage.emergencycall.activities.LoginActivity;
import br.com.interage.emergencycall.model.User;

public class SessionManager {
    // Shared Preferences
    SharedPreferences pref;

    // Editor for Shared preferences
    Editor editor;

    // Context
    Context _context;

    // Shared pref mode
    int PRIVATE_MODE = 0;

    // Sharedpref file name
    private static final String PREF_NAME = "EmergencyCallPref";

    // All Shared Preferences Keys
    private static final String IS_LOGIN = "IsLoggedIn";

    // User name
    public static final String KEY_NAME = "name";

    // Id
    public static final String KEY_ID = "id";

    // Federal Registration
    public static final String KEY_CPF = "cpf";

    // Email
    public static final String KEY_EMAIL = "email";

    // Id GCM User
    public static final String KEY_ID_GCM_USER = "idGcmUser";

    // If create a shortcut, put true, else put false
    public static final String KEY_SHORTCUT_ICON = "shortcut_icon";

    // Constructor
    public SessionManager(Context context) {
        this._context = context;
        pref = _context.getSharedPreferences(PREF_NAME, PRIVATE_MODE);
        editor = pref.edit();
    }

    /**
     * Create Login session
     *
     * @param user
     */
    public void createLoginSession(User user) {
        // Storing login value as TRUE
        editor.putBoolean(IS_LOGIN, true);

        // Storing name in pref
        editor.putString(KEY_NAME, user.getName());

        // Storing email in pref
        editor.putString(KEY_CPF, user.getCpf());

        editor.putLong(KEY_ID, user.getId());

        editor.putString(KEY_EMAIL, user.getEmail());

        // commit changes
        editor.commit();
    }

    /**
     * Check login method will check user login status If false it will redirect
     * user to login page Else won't do anything.
     */
    public void checkLogin() {
        // Check login status
        if (!this.isLoggedIn()) {
            Toast.makeText(_context, "Por favor, faça login novamente.",
                    Toast.LENGTH_SHORT).show();
            // user is not logged in redirect him to Login Activity
            Intent i = new Intent(_context, LoginActivity.class);
            // Closing all the Activities
            i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);

            // Add new Flag to start new Activity
            i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);

            // Staring Login Activity
            _context.startActivity(i);
        }

    }

    /**
     * Get stored session data
     */
    public User getUserDetails() {
        User user = new User();
        // user id
        user.setId(pref.getLong(KEY_ID, 0l));

        // user name
        user.setName(pref.getString(KEY_NAME, "Usuário não conectado."));

        // user federalRegistration
        user.setCpf(pref.getString(KEY_CPF, ""));

        // return user
        return user;
    }

    /**
     * Clear session details
     */
    public void logoutUser() {
        // Clearing all data from Shared Preferences
        editor.clear();
        editor.commit();

        // After logout redirect user to Login Activity
        Intent i = new Intent(_context, LoginActivity.class);
        // Closing all the Activities
        i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);

        // Add new Flag to start new Activity
        i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);

        // Staring Login Activity
        _context.startActivity(i);
    }

    /**
     * Quick check for login
     * *
     */
    // Get Login State
    public boolean isLoggedIn() {
        return pref.getBoolean(IS_LOGIN, false);
    }

    /**
     * Add BOOLEAN params to session.
     *
     * @param param
     * @param value
     */
    public void addSessionParam(String param, Boolean value) {
        editor.putBoolean(param, value);
        editor.commit();
    }

    /**
     * Add STRING params to session.
     *
     * @param param
     * @param value
     */
    public void addSessionParam(String param, String value) {
        editor.putString(param, value);
        editor.commit();
    }

    /**
     * Add LONG params to session.
     *
     * @param param
     * @param value
     */
    public void addSessionParam(String param, Long value) {
        editor.putLong(param, value);
        editor.commit();
    }

    /**
     * Return a boolean session param from session
     * @param paramName
     * @return
     */
    public Boolean getSessionParam (String paramName) {
        return pref.getBoolean(paramName, false);
    }
}