package br.com.interage.emergencycall.model;

import android.content.Context;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import br.com.interage.emergencycall.R;

/**
 * Created by lacroiix on 03/08/15.
 */
public class Occurrence implements Serializable {

    private Long id;
    private String name;
    private int icon;

    public Occurrence () {

    }

    public Occurrence(Long id, String name, int icon) {
        this.id = id;
        this.name = name;
        this.icon = icon;
    }

    public static List<Occurrence> policyOccurrences(Context c) {
        List<Occurrence> occurrences = new ArrayList<Occurrence>();
        occurrences.add(new Occurrence(1l, "AGRESSÃO", R.drawable.ic_launcher));
        occurrences.add(new Occurrence(2l, "ASSALTO", R.drawable.ic_launcher));
        occurrences.add(new Occurrence(3l, "ASSALTO ARMADO", R.drawable.ic_launcher));
        occurrences.add(new Occurrence(4l, "INVASÃO", R.drawable.ic_launcher));
        return occurrences;
    }

    public static List<Occurrence> samuOccurrences(Context c) {
        List<Occurrence> occurrences = new ArrayList<Occurrence>();
        occurrences.add(new Occurrence(1l, "ATROPELAMENTO", R.drawable.ic_launcher));
        occurrences.add(new Occurrence(2l, "DESMAIO", R.drawable.ic_launcher));
        occurrences.add(new Occurrence(3l, "QUEIMADURAS", R.drawable.ic_launcher));
        occurrences.add(new Occurrence(4l, "CHOQUE ELÉTRICO", R.drawable.ic_launcher));
        occurrences.add(new Occurrence(5l, "INTOXICAÇÃO", R.drawable.ic_launcher));
        return occurrences;
    }

    public static List<Occurrence> stransOccurrences(Context c) {
        List<Occurrence> occurrences = new ArrayList<Occurrence>();
        occurrences.add(new Occurrence(1l, "COLISÃO", R.drawable.ic_launcher));
        occurrences.add(new Occurrence(2l, "CAPOTAMENTO", R.drawable.ic_launcher));
        occurrences.add(new Occurrence(3l, "SEMÁFORO DESLIGADO", R.drawable.ic_launcher));
        occurrences.add(new Occurrence(4l, "ESTACIONAMENTO INDEVIDO", R.drawable.ic_launcher));
        return occurrences;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getIcon() {
        return icon;
    }

    public void setIcon(int icon) {
        this.icon = icon;
    }
}
