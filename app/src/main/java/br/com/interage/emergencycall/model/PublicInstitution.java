package br.com.interage.emergencycall.model;

import android.content.Context;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import br.com.interage.emergencycall.R;

/**
 * Created by lacroiix on 03/08/15.
 */
public class PublicInstitution implements Serializable {

    // Key phone number
    public static final int KEY_PHONE_STRANS = 190;
    public static final int KEY_PHONE_POLICY = 190;
    public static final int KEY_PHONE_SAMU = 192;

    // Key Name
    public static final String KEY_NAME_STRANS = "Strans";
    public static final String KEY_NAME_POLICY = "Polícia";
    public static final String KEY_NAME_SAMU = "SAMU";

    private Long id;
    private String name;
    private String description;
    private List<Occurrence> occurrences;
    private int phoneNumber;
    private int icon;
    private int background;

    public static List<PublicInstitution> returnList(Context c) {
        List<PublicInstitution> list = new ArrayList<PublicInstitution>();

        PublicInstitution pi = new PublicInstitution();
        pi.setName("POLÍCIA");
        pi.setId(1l);
        pi.setIcon(R.drawable.ic_policy_wht);
        pi.setBackground(R.drawable.background_policy);
        pi.setOccurrences(Occurrence.policyOccurrences(c));
        list.add(pi);

        pi = new PublicInstitution();
        pi.setName("SAMU");
        pi.setId(2l);
        pi.setIcon(R.drawable.ic_hospital_wht);
        pi.setBackground(R.drawable.background_samu);
        pi.setOccurrences(Occurrence.samuOccurrences(c));
        list.add(pi);

        pi = new PublicInstitution();
        pi.setName("STRANS");
        pi.setId(3l);
        pi.setIcon(R.drawable.ic_traffic_light_wht);
        pi.setBackground(R.drawable.background_strans);
        pi.setOccurrences(Occurrence.stransOccurrences(c));
        list.add(pi);

        return list;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(int phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public List<Occurrence> getOccurrences() {
        return occurrences;
    }

    public void setOccurrences(List<Occurrence> occurrence) {
        this.occurrences = occurrence;
    }

    public int getIcon() {
        return icon;
    }

    public void setIcon(int icon) {
        this.icon = icon;
    }

    public int getBackground() {
        return background;
    }

    public void setBackground(int background) {
        this.background = background;
    }
}
