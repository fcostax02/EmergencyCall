package br.com.interage.emergencycall.model;

/**
 * Created by lacroiix on 29/07/15.
 */
public class User {

    private Long id;
    private String name;
    private String cpf;
    private String idGcmUser;
    private String email;

    public User() {
    }

    public User(Long id, String name, String cpf, String email) {
        this.id = id;
        this.name = name;
        this.cpf = cpf;
        this.email = email;
    }

    public static User getDefaultUser() {
        return new User(999666l, "Usuario nao identificado", "00000000000", "email nao " +
                "identificado");
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getIdGcmUser() {
        return idGcmUser;
    }

    public void setIdGcmUser(String idGcmUser) {
        this.idGcmUser = idGcmUser;
    }

    public String getCpf() {
        return cpf;
    }

    public void setCpf(String cpf) {
        this.cpf = cpf;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
}
