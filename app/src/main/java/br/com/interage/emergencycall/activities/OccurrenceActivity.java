package br.com.interage.emergencycall.activities;

import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ListView;

import java.util.List;

import br.com.interage.emergencycall.R;
import br.com.interage.emergencycall.adapters.PublicInstitutionOccurrenceAdapter;
import br.com.interage.emergencycall.model.Occurrence;
import br.com.interage.emergencycall.model.PublicInstitution;
import br.com.interage.emergencycall.utils.SessionManager;

public class OccurrenceActivity extends ActionBarActivity {

    private ListView occurrenceList;
    private List<Occurrence> occurrences;
    SessionManager session;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_occurrence);

        // Set ActionBar HomeButton and icon
        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);

        session = new SessionManager(this);
        session.checkLogin();

        occurrences = getObjectFromBundle().getOccurrences();

        occurrenceList = (ListView) findViewById(R.id.lv_occurrence_list);
        PublicInstitutionOccurrenceAdapter occurrenceAdapter = new
                PublicInstitutionOccurrenceAdapter(this, occurrences);
        occurrenceList.setAdapter(occurrenceAdapter);

    }

    private PublicInstitution getObjectFromBundle() {
        Bundle bundle = getIntent().getBundleExtra("extraPi");
        return (PublicInstitution) bundle.getSerializable("publicInstitution");
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_occurrence, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
