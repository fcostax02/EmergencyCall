package br.com.interage.emergencycall.services;

import android.app.IntentService;
import android.content.Context;
import android.content.Intent;
import android.os.Handler;

import br.com.interage.emergencycall.activities.MainActivity;
import br.com.interage.emergencycall.R;

/**
 * Created by lacroiix on 26/07/15.
 */
public class MainService extends IntentService {

    private Handler handler = new Handler();
    private static Context staticContext;

    /**
     * Creates an IntentService.  Invoked by your subclass's constructor.
     */
    public MainService() {
        super("MainService");
        this.staticContext = this;
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        handler.post(r);
    }

    Runnable r = new Runnable() {
        @Override
        public void run() {
            shortcutIcon();
        }
    };

    public static void shortcutIcon(){

        Intent shortcutIntent = new Intent(staticContext, MainActivity.class);
        shortcutIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        shortcutIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);

        Intent addIntent = new Intent();
        addIntent.putExtra(Intent.EXTRA_SHORTCUT_INTENT, shortcutIntent);
        addIntent.putExtra(Intent.EXTRA_SHORTCUT_NAME, "EmergencyCall!");
        addIntent.putExtra(Intent.EXTRA_SHORTCUT_ICON_RESOURCE, Intent.ShortcutIconResource.fromContext(staticContext, R.drawable.abc_ic_voice_search_api_mtrl_alpha));
        addIntent.setAction("com.android.launcher.action.INSTALL_SHORTCUT");
        staticContext.sendBroadcast(addIntent);
    }
}
