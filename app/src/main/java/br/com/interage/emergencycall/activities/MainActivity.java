package br.com.interage.emergencycall.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import br.com.interage.emergencycall.R;
import br.com.interage.emergencycall.adapters.PublicInstitutionAdapter;
import br.com.interage.emergencycall.model.PublicInstitution;
import br.com.interage.emergencycall.utils.SessionManager;


public class MainActivity extends ActionBarActivity {

    private ListView lv_publicInstitutions;
    SessionManager session;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        session = new SessionManager(this);
        session.checkLogin();

        // startService(new Intent(this, MainService.class));
        if (!session.getSessionParam(SessionManager.KEY_SHORTCUT_ICON)) {
            shortcutIcon();
        }

        lv_publicInstitutions = (ListView) findViewById(R.id.lv_main_institutions);
        lv_publicInstitutions.setAdapter(new PublicInstitutionAdapter(this,
                PublicInstitution.returnList(this)));

        // Implementando o Click no Item da Lista
        lv_publicInstitutions.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Bundle extraBundle = new Bundle();
                extraBundle.putSerializable("publicInstitution", PublicInstitution.returnList
                        (MainActivity.this).get(position));

                startActivity(new Intent(MainActivity.this, OccurrenceActivity.class).
                        putExtra("extraPi", extraBundle));
            }
        });
    }

    public void shortcutIcon() {

        Intent shortcutIntent = new Intent(this, MainActivity.class);
        shortcutIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        shortcutIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);

        try {
            Intent addIntent = new Intent();
            addIntent.putExtra(Intent.EXTRA_SHORTCUT_INTENT, shortcutIntent);
            addIntent.putExtra(Intent.EXTRA_SHORTCUT_NAME, "EmergencyCall!");
            addIntent.putExtra(Intent.EXTRA_SHORTCUT_ICON_RESOURCE, Intent.ShortcutIconResource.fromContext(this, R.drawable.ic_launcher));
            addIntent.setAction("com.android.launcher.action.INSTALL_SHORTCUT");
            this.sendBroadcast(addIntent);
            session.addSessionParam(SessionManager.KEY_SHORTCUT_ICON, true);
        } catch (Exception e) {
            e.printStackTrace();
            session.addSessionParam(SessionManager.KEY_SHORTCUT_ICON, false);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

}
