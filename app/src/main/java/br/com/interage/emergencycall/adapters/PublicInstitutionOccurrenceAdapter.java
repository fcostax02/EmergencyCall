package br.com.interage.emergencycall.adapters;

import android.annotation.TargetApi;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.util.List;

import br.com.interage.emergencycall.R;
import br.com.interage.emergencycall.model.Occurrence;

/**
 * Created by lacroiix on 03/08/15.
 */
public class PublicInstitutionOccurrenceAdapter extends BaseAdapter {

    private List<Occurrence> list;
    private Context context;
    private LayoutInflater mLayoutInflater = null;
    private int quantityOcc;

    public PublicInstitutionOccurrenceAdapter(Context c, List<Occurrence> l) {
        list = l;
        context = c;
        quantityOcc = l.size();
        mLayoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int position) {
        return list.get(position);
    }

    @Override
    public long getItemId(int position) {
        return list.get(position).getId();
    }

    @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        View rowView = null;
        int sum = 0;
        sum = position % 2;

        if (sum == 0) {
            rowView = mLayoutInflater.inflate(R.layout.item_occurrence_layout, parent, false);

            RelativeLayout rl_option1 = (RelativeLayout) rowView.findViewById(R.id.rl_option1);
            RelativeLayout rl_option2 = (RelativeLayout) rowView.findViewById(R.id.rl_option2);
            TextView tv_occurrence1 = (TextView) rowView.findViewById(R.id.tv_occurrence1);
            TextView tv_occurrence2 = (TextView) rowView.findViewById(R.id.tv_occurrence2);

            rl_option1.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    dialog(position).show();
                }
            });

            tv_occurrence1.setText(list.get(position).getName());
            Drawable icon1 = context.getResources().getDrawable(list.get(position).getIcon());
            tv_occurrence1.setCompoundDrawablesWithIntrinsicBounds(null, icon1, null, null);

            // Verifica se há outro componente para ser exibido
            if (quantityOcc > (position + 1)) {
                tv_occurrence2.setText(list.get(position + 1).getName());
                Drawable icon2 = context.getResources().getDrawable(list.get(position + 1).getIcon());
                tv_occurrence2.setCompoundDrawablesWithIntrinsicBounds(null, icon2, null, null);

                rl_option2.setOnClickListener(new View.OnClickListener() {

                    @Override
                    public void onClick(View v) {
                        dialog(position + 1).show();
                    }
                });
            }
        } else {
            rowView = mLayoutInflater.inflate(R.layout.item_null_occurrence_layout, parent, false);
        }

        return rowView;
    }

    private AlertDialog.Builder dialog(int position) {
        final int itemPosition = position;

        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        return builder.setIcon(R.mipmap.ic_launcher)
                .setTitle("ATENÇÃO!!")
                .setMessage("VOCÊ TEM CERTEZA QUE GOSTARIA DE SOLICITAR ESTE SERVIÇO?")
                .setPositiveButton("SIM", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        callTheServer(itemPosition);
                    }
                })
                .setNegativeButton("NÃO", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        // NÃO FAZ NADA
                    }
                });
    }

    private void callTheServer(int position) {
        Occurrence entity = new Occurrence();
        entity.setName(list.get(position).getName());
        entity.setId(list.get(position).getId());
        Toast.makeText(context, "Enviando para o servidor\nAguarde", Toast.LENGTH_SHORT).show();

    }
}
