package br.com.interage.emergencycall.adapters;

import android.annotation.TargetApi;
import android.content.Context;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.List;

import br.com.interage.emergencycall.R;
import br.com.interage.emergencycall.model.PublicInstitution;

/**
 * Created by lacroiix on 03/08/15.
 */
public class PublicInstitutionAdapter extends BaseAdapter {

    private List<PublicInstitution> list;
    private Context context;
    private LayoutInflater mLayoutInflater = null;

    public PublicInstitutionAdapter(Context c, List<PublicInstitution> l) {
        list = l;
        context = c;
        mLayoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int position) {
        return list.get(position);
    }

    @Override
    public long getItemId(int position) {
        return list.get(position).getId();
    }

    @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View rowView = mLayoutInflater.inflate(R.layout.item_layout_institution, parent, false);

        final TextView tv_pi_name = (TextView) rowView.findViewById(R.id.tv_pi_name);
        final TextView tv_pi_occurrence = (TextView) rowView.findViewById(R.id.tv_pi_occurrence);
        final RelativeLayout rl_pi_mainLayout = (RelativeLayout) rowView.findViewById(R.id.rl_pi_mainLayout);

        Drawable icon = context.getResources().getDrawable(list.get(position).getIcon());
        Drawable background = context.getResources().getDrawable(list.get(position).getBackground());

        tv_pi_name.setText(list.get(position).getName());
        // seta a propriedade: drawableTop
        tv_pi_name.setCompoundDrawablesWithIntrinsicBounds(null, icon, null, null);
        tv_pi_occurrence.setText(list.get(position).getOccurrences().size() + " SITUAÇÕES");
        rl_pi_mainLayout.setBackground(background);

        return rowView;
    }
}
