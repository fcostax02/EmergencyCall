package br.com.interage.emergencycall.activities;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import br.com.interage.emergencycall.R;
import br.com.interage.emergencycall.model.User;
import br.com.interage.emergencycall.utils.Mask;
import br.com.interage.emergencycall.utils.SessionManager;
import br.com.interage.emergencycall.utils.Validator;
import br.com.interage.emergencycall.utils.ValidatorCPF;

public class LoginActivity extends Activity {

    private Button btn_confirm;
    private EditText et_cpf;
    private EditText et_senha;

    SessionManager session;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        session = new SessionManager(this);

        initViews();
    }

    private void initViews() {
        btn_confirm = (Button) findViewById(R.id.btn_login_confirm);
        et_cpf = (EditText) findViewById(R.id.et_login_cpf);
        //Coloca a máscara de cpf
        et_cpf.addTextChangedListener(Mask.insert("###.###.###-##", et_cpf));
        et_senha = (EditText) findViewById(R.id.et_login_senha);
    }

    public void login(View v) {
        boolean validate;
        validate = Validator.validateNotNull(this, et_cpf);
        validate &= Validator.validateNotNull(this, et_senha);
        if (Valid() && validate) {
            startActivity(new Intent(this, MainActivity.class));
            finish();
        }
    }

    /**
     * Verifica se o cpf digitado eh válido.
     *
     * @return boolean
     */
    private boolean Valid() {
        String cpf = Mask.unmask(et_cpf.getText().toString());
        if (!ValidatorCPF.check(cpf)) {
            et_cpf.setError(this.getString(R.string.error_field_required));
            return false;
        }
        return true;
    }

    public void forgotPassword(View v) {
        //TODO XXX Implementar a tela de esqueci a senha.
    }

    public void accessWithoutLogin(View v) {
        session.createLoginSession(User.getDefaultUser());
        startActivity(new Intent(this, MainActivity.class));
        finish();
    }

    public void newAccount(View v) {
        //TODO XXX Implementar a tela de registro no app.
    }

}
